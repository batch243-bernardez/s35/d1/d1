const express = require("express");

const app = express();

// Import Mongoose - it is a package that allows creation of schemas to model our data structures. It also has access to a number of methods for database manipulation.
const mongoose = require("mongoose");

const port = 3001;


// MongoDB Connection
/*
	- Syntax:
		mongoose.connect("connectionString")

	- "useNewUrlParser" - allows us to avoid any current or future errors while connecting to MongoDB
	- "useUnifiedTopology" - removing the support to several connections that are not relevant 
*/
	mongoose.connect("mongodb+srv://mpyb:mpyb@zuittbatch243.kotcmj5.mongodb.net/?retryWrites=true&w=majority",
			{ 
				useNewUrlParser : true,  
				useUnifiedTopology : true
			}
		);


// Set notification for connection success or failure

	let db = mongoose.connection;

/*
	- .on() - will check if there is an error
	- console.error.bind() - print errors in console browsers
	- .once() - 
*/
	db.on("error", console.error.bind(console, "connection error"));
	db.once("open", () => console.log("We're connected to the cloud database"));


// Mongoose Schema
/*
	- It determines the structure of the documents to be written in the database. It also acts as a blueprint.
	- Use the Schema() constructor of the Mongoose module to create a new schema object
*/
	const taskSchema = new mongoose.Schema({ 
		name : String,
		status : { 
			type : String,
			default : "pending"
		}
	});


// Models
/*
	- This uses schemas
	- To create objects corresponding to the schema (blueprint)
	- Acts as a middleman from the server (JS code) to our database
	- Server > Schema > Database > Collection

	- Follows the MVC (model-view-control) approach for naming convention
*/
	const Task = mongoose.model("Task", taskSchema);

	app.use(express.json());
	app.use(express.urlencoded({extended:true}))



// Create a POST route (for new task)
/*
	Business Logic:
	1. Add a functionality to check if there are duplicate tasks
		- If the task already exists in the database, we return an error
		- If the task doesn't exist in the database, we add it in the database
	2. The task data will be coming from the request's body
	3. Create a new Task object with a "name" field/property
	4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object

	- "findOne" - mongoose method that acts similar to 'find' in MongoDB
*/
	app.post("/tasks", (req, res) => {
		Task.findOne({name : req.body.name}, (err, result) => {
			
			// If a document was found and the document name matches the information sent via postman
			if(result != null && result.name == req.body.name){
				
				// Return a message to the client or postman
				return res.send("Duplicate task found");
			}
			else{
				let newTask = new Task({
					name : req.body.name
			});

				// .save() - will store the information to the database
				newTask.save((saveErr, savedTask) => {
					
					// If there are errors in saving
					if(saveErr){
						return console.error(saveErr);
					}

					// No error found while creating the document
					else{
						return res.status(201).send("New task created");
					}
				})
			}
		})
	})


// Create 

/*
	Business Logic

	1. Retrieve all the documents
	2. If an error is encountered, print the error
	3. If no errors are found, send a success status back to the client or postman and return an array of documents
*/

app.get("/tasks",(req, res) => {
	Task.find({}, (err, result) =>{
		if(err){
			return console.log(err);
		}
		else {
			return res.status(200).json({
				data:result
			})
		}
	})
}) 

app.listen(port, () => console.log(`Server running at port ${port}`));